# Talking-post

It's the "Listening Post", which doesn't actually listen but talks.

Listening Post V2.1 will be connected to the LoRaWan network to provide metadata.

# Required Library
[https://github.com/PowerBroker2/DFPlayerMini_Fast](https://github.com/PowerBroker2/DFPlayerMini_Fast)

## V1.0 Hardware

*  SparkFun Ardunio Uno ATMEGA328-P
*  DFPlayer Mini & SD Card
*  4xAA Batterys
*  Speaker
*  Button

## V2.0 Hardware

*  ATMEGA328-P on Protoboard
*  DFPlayer Mini & SD Card
*  CCCV Controller
*  1.5W Solar cell
*  18650 Lithium-ion Cell 2000MaH
*  Speaker
*  Button


## V2.1 Hardware

*  ATMEGA328-P on Protoboard
*  DFPlayer Mini & SD Card
*  LoRa SX1276 Module
*  Lithium charging controller MCP73832T
*  Panasonic 18650PF 2900MaH
*  1N5342B 5W 6.8V Diode
*  2W Speaker
*  Button
*  6V 333MaH Solar Cell | 6V 500MaH Solar Cell | Plugged in (LM2695S Voltage Regulator)
